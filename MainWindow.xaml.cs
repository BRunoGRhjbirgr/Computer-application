﻿using StroyMaterial.Helpers;
using System.Windows;

namespace StroyMaterial
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            PageHelper.PageName = pageName;
            PageHelper.MainFrame = mainFrame;
            PageHelper.MainFrame.Navigate(new Pages.SignIn());
        }
    }
}
