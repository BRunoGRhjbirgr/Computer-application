﻿using StroyMaterial.DataBase;
using StroyMaterial.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StroyMaterial.Pages
{
    /// <summary>
    /// Логика взаимодействия для AddPage.xaml
    /// </summary>
    public partial class AddPage : Page
    {
        public AddPage()
        {
            InitializeComponent();
            PageHelper.PageName.Text = "Добавление товара";
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.GoBack();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Product prod = new Product();
            prod.ProductArticleNumber = tArticul.Text;
            prod.ProductName = tName.Text;
            prod.ProductDescription = tDescription.Text;
            prod.ProductCategory = tCat.Text;
            prod.ProductManufacturer = tManufactur.Text;
            prod.ProductCost = Convert.ToDecimal(tCost.Text);
            prod.ProductImage = tImage.Text;
            prod.ProductUnit = tUnit.Text;
            prod.ProductQuantityInStock = Convert.ToInt32(tCount.Text);
           
                             
                                                       

            if (MessageBoxResult.Yes == MessageBox.Show("Вы действительно хотите добавить запись?", "Предупреждение", MessageBoxButton.YesNo))
            {
                PageHelper.ConnectDB.Product.Add(prod);
                PageHelper.ConnectDB.SaveChangesAsync();
            }
        }
    }
}
