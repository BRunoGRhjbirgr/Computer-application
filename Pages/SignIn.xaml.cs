﻿using StroyMaterial.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StroyMaterial.Pages
{
    /// <summary>
    /// Логика взаимодействия для SignIn.xaml
    /// </summary>
    public partial class SignIn : Page
    {
        public SignIn()
        {
            InitializeComponent();
            PageHelper.PageName.Text = "Авторизация";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           

            if (PageHelper.ConnectDB.User.Where(x => x.UserLogin == tLogin.Text && x.UserPassword == tPass.Password).FirstOrDefault() == null)
            {
                MessageBox.Show("Неверные данные", "Ошибка входа");
                return;
            }

            if (PageHelper.ConnectDB.User.Where(x => x.UserLogin == tLogin.Text && x.UserPassword == tPass.Password && x.UserRole == 1).FirstOrDefault() != null)
            {
                PageHelper.role = 3;   //Администратор
                PageHelper.MainFrame.Navigate(new Pages.ProductList());
            }

            if (PageHelper.ConnectDB.User.Where(x => x.UserLogin == tLogin.Text && x.UserPassword == tPass.Password && x.UserRole == 2).FirstOrDefault() != null)
            {
                PageHelper.role = 2;  //Менеджер
                PageHelper.MainFrame.Navigate(new Pages.ProductList());
            }

            if (PageHelper.ConnectDB.User.Where(x => x.UserLogin == tLogin.Text && x.UserPassword == tPass.Password && x.UserRole == 2).FirstOrDefault() != null)
            {
                PageHelper.role = 1; //Авторизованный человек
                PageHelper.MainFrame.Navigate(new Pages.ProductList());
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Вы точно хотите войти без авторизации ?", "Предупреждение", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                PageHelper.MainFrame.Navigate(new Pages.ProductList());
                PageHelper.role = 0; //Неавторизованный человек
            }

            else
            {
                return;
            }
        }

        private void tLogin_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
