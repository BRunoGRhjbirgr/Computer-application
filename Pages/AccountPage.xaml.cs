﻿using StroyMaterial.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StroyMaterial.Pages
{
    /// <summary>
    /// Логика взаимодействия для AccountPage.xaml
    /// </summary>
    public partial class AccountPage : Page
    {
        public AccountPage()
        {
            InitializeComponent();
            PageHelper.PageName.Text = "Аккаунты";
            var user = PageHelper.ConnectDB.User;

            tListAccount.ItemsSource = user.ToList();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.GoBack();
        }

        private void btnRecord_Click(object sender, RoutedEventArgs e)
        {
           

            if (MessageBoxResult.Yes == MessageBox.Show("Вы действительно хотите применить изменения ?", "Предупреждение", MessageBoxButton.YesNo))
            {
                PageHelper.ConnectDB.SaveChanges();
            }
        }
    }
}
